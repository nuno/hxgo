package cache

// Cache interface
type Interface interface {
	Set(key string, value interface {}, expire int)
	Get(key string) interface {}
	Del(key string)
	Has(key string) bool
	Clear()
	Gc()
}

